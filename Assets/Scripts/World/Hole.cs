﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour {

    [SerializeField] private float TimeRequiredToAdvance = 3f;
    [SerializeField] private Transform NextHole;

    private Dictionary<GameObject, float> _timeInHole = new Dictionary<GameObject, float>();

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.layer == 9) {
            if (!_timeInHole.ContainsKey(other.gameObject)) {
                _timeInHole.Add(other.gameObject, 0);
            }
            _timeInHole[other.gameObject] += Time.deltaTime;
            if (_timeInHole[other.gameObject] >= TimeRequiredToAdvance) {
                if (NextHole == null) {
                    SceneManager.Instance.LoadScene(SceneManager.MainMenuScene);
                }
                else {
                    GameManager.Instance.SetHoleDetails(NextHole.parent.gameObject.GetComponentInParent<HoleDetails>());
                    GameManager.Instance.Strokes = 0;
                    other.gameObject.transform.position = NextHole.position;
                    other.gameObject.transform.rotation = Quaternion.identity;
                    other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    _timeInHole.Remove(other.gameObject);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        _timeInHole.Remove(other.gameObject);
    }
}
