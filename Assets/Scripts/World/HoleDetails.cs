﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleDetails : MonoBehaviour {

    [SerializeField] private int _par = 2;
    [SerializeField] private int _maxStrokes = 7;

    public int Par { get { return _par; } }
    public int MaxStrokes { get { return _maxStrokes; } }

}
