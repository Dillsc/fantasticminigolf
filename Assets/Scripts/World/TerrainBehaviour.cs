﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainBehaviour : MonoBehaviour {

    private List<GameObject> playerList = new List<GameObject>();

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 9) {
            playerList.Add(collision.gameObject);
            StartCoroutine(ResetPlayer(collision.gameObject));
        }
    }

    private void OnCollisionExit(Collision collision) {
        if (collision.gameObject.layer == 9) {
            playerList.Remove(collision.gameObject);
        }
    }

    private IEnumerator ResetPlayer(GameObject player) {
        yield return new WaitForSeconds(5f);

        if (playerList.Contains(player)) { 
            Player p = player.GetComponent<Player>();
            p.MoveToPreviousPosition();
        }
    }
}
