﻿using UniRx;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour {

    [Header("Components")]
    [SerializeField] private Transform CameraFollowTarget = null;
    [SerializeField] private AudioSource AudioSource = null;
    [SerializeField] private GameObject DirectionObject = null;
    [SerializeField] private GameObject PointerObject = null;

    [Header("Player")]
    [SerializeField] private AudioClip CollisionSound = null;
    [SerializeField] private float _MaxPower = 10;
    [SerializeField] private float SpeedDeadZone = 0f;

    [Header("Tutorial")]
    [SerializeField] private float TimerCanProgressDelay = 1f;
    [SerializeField] private string TutorialText1 = "Hello and welcome to Golf game!";

    private int _TutorialState = -1;
    private float _TutorialCanProgressTimer;
    private bool _TutorialCanProgress = false;

    private AudioSource _PlayerAudioSource;
    private Rigidbody _Body;
    private ReactiveProperty<float> _Power = new ReactiveProperty<float>(0);
    private ReactiveProperty<float> _PreviousPower = new ReactiveProperty<float>(0);
    private Vector3 _StartPosition;
    private Vector3 _PreviousPosition;
    private bool _InHole = false;

    public AudioSource PlayerAudioSource { get { return _PlayerAudioSource; } }
    public Rigidbody Body { get { return _Body; } }
    public float Power { get { return _Power.Value; } set { _Power.Value = value; } }
    public ReactiveProperty<float> PowerReactiveProperty { get { return _Power; } }
    public float PreviousPower { get { return _PreviousPower.Value; } set { _PreviousPower.Value = value; } }
    public ReactiveProperty<float> PreviousPowerReactiveProperty { get { return _PreviousPower; } }
    public Vector3 StartPosition { get { return _StartPosition; } }
    public Vector3 PreviousPosition { get { return _PreviousPosition; } set { _PreviousPosition = value; } }
    public float MaxPower { get { return _MaxPower; } }
    public bool InHole { get { return _InHole; } set { _InHole = value; } }

    public void OnCollisionEnter(Collision collision) {
        SoundManager.Instance.PlaySoundAtLocation(transform.position, CollisionSound, new SourceSettings().SetVolume(Body.velocity.magnitude / 16));
    }

    private void Start() {
        CameraManager.Instance.SetFollowTransform(CameraFollowTarget);
        CameraManager.Instance.IgnoredColliders = GetComponentsInChildren<Collider>();

        _StartPosition = transform.position;
        _PreviousPosition = transform.position;
        _Body = GetComponent<Rigidbody>();
    }

    public void MoveToPreviousPosition() {
        transform.position = _PreviousPosition;
        Body.velocity = Vector3.zero;
    }

    private void Update() {
        if (transform.position.y <= -50f) {
            MoveToPreviousPosition();
        }

        if (Input.GetKeyDown(KeyCode.RightAlt)) {
            Body.velocity = Vector3.zero;
        }

        if (Input.GetMouseButtonDown(0) && Cursor.lockState != CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            return;
        }

        CameraFollowTarget.rotation = Quaternion.identity;
        CameraFollowTarget.position = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);

        if (Input.GetKey(KeyCode.Space)) {
            float mouseAxisUp = Input.GetAxisRaw("Mouse Y");
            HandleCameraInput(Input.GetAxisRaw("Mouse X"), 0);
            if (Body.velocity.magnitude <= SpeedDeadZone) {
                AddPower(mouseAxisUp);
            }
            UpdateDirectionHandler();
        } else {
            DirectionObject.SetActive(false);
            HandleCameraInput(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        }

        if (Input.GetKeyUp(KeyCode.Space)) {
            Shoot();
        }
    }

    public void AddPower(float p) {
        Power += p * 0.25f;

        if (Power > MaxPower) {
            Power = MaxPower;
        }
        if (Power < 0) {
            Power = 0;
        }
    }

    public void UpdateDirectionHandler() {
        DirectionObject.SetActive(true);
        PointerObject.transform.localScale = new Vector3(Power * 0.5f, 1, 0.02f);
        DirectionObject.transform.rotation = Quaternion.Euler(new Vector3(0, CameraManager.Instance.transform.rotation.eulerAngles.y - 90, 0));
    }

    public void Shoot() {
        if (Power <= 0) {
            return;
        }

        GameManager.Instance.Strokes += 1;

        PreviousPosition = transform.position;

        Vector3 playerPos = transform.position;
        Vector3 cameraPos = CameraManager.Instance.transform.position;

        playerPos.y = 0;
        cameraPos.y = 0;

        Vector3 direction = playerPos - cameraPos;

        Body.AddForce(direction.normalized * Power, ForceMode.Impulse);

        PreviousPower = Power;
        Power = 0;
    }

    private void HandleCameraInput(float mouseAxisRight, float mouseAxisUp) {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (Cursor.visible) {
                Application.Quit();
            }
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        Vector3 lookInputVector = new Vector3(mouseAxisRight * 0.1f, mouseAxisUp * 0.1f, 0f);

        if (Cursor.lockState != CursorLockMode.Locked) {
            lookInputVector = Vector3.zero;
        }

        float scrollInput = 0f;
        if (!CameraManager.Instance.LockToFirstPersonPerspective) {
            scrollInput = -Input.GetAxis("Mouse ScrollWheel");
        }

        CameraManager.Instance.UpdateWithInput(Time.deltaTime, scrollInput, lookInputVector);
    }
}