﻿using Discord;
using System;
using UnityEngine;

public class DiscordController : Singleton<DiscordController> {

    private DiscordRPC.RichPresence presence;
    private DiscordRPC.EventHandlers handlers;

    [Header("Discord RPC Settings")]
    [SerializeField] private string applicationId = null;
    [SerializeField] public string optionalSteamId = null;
    [SerializeField] public int callbackCalls = 0;

    private bool running = false;

    private void OnDestroy() {
        Shutdown();
    }

    public DiscordController Startup() {
        if (!running) {
            DontDestroyOnLoad(gameObject);
            presence = new DiscordRPC.RichPresence();
            DiscordRPC.Initialize(applicationId, ref handlers, true, optionalSteamId);
            running = true;
        }
        return this;
    }

    public void Shutdown() {
        if (running) {
            DiscordRPC.Shutdown();
            running = false;
        }
    }

    public void LoadPreset(DiscordPreset preset) {
        presence.details = preset.Details;
        presence.state = preset.State;
        presence.startTimestamp = preset.SetStartTimeNow ? (Int32) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds : preset.StartTime;
        presence.endTimestamp = preset.EndTime;
        presence.largeImageKey = preset.LargeImageKey;
        presence.largeImageText = preset.LargeImageText;
        presence.smallImageKey = preset.SmallImageKey;
        presence.smallImageText = preset.SmallImageText;
        presence.partyId = preset.PartyID;
        presence.partySize = preset.PartySize;
        presence.partyMax = preset.PartyMax;
        presence.matchSecret = preset.MatchSecret;
        presence.joinSecret = preset.JoinSecret;
        presence.spectateSecret = preset.SpectateSecret;
        presence.instance = preset.isInstance;
        refresh();
    }

    public void SetState(string state) {
        presence.state = state;
        refresh();
    }

    public void SetDetails(string details) {
        presence.details = details;
        refresh();
    }

    public void SetStartTimestamp(long startTimestamp) {
        presence.startTimestamp = startTimestamp;
        refresh();
    }

    public void SetStartTimestampToNow() {
        SetStartTimestamp((Int32) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
    }

    public void SetEndStartTimestamp(long endTimestamp) {
        presence.endTimestamp = endTimestamp;
        refresh();
    }

    public void SetLargeImageKey(string largeImageKey) {
        presence.largeImageKey = largeImageKey;
        refresh();
    }

    public void SetLargeImageText(string largeImageText) {
        presence.largeImageText = largeImageText;
        refresh();
    }

    public void SetSmallImageKey(string smallImageKey) {
        presence.smallImageKey = smallImageKey;
        refresh();
    }

    public void SetSmallImageText(string smallImageText) {
        presence.smallImageText = smallImageText;
        refresh();
    }

    public void SetPartyId(string partyId) {
        presence.partyId = partyId;
        refresh();
    }

    public void SetPartySize(int partySize) {
        presence.partySize = partySize;
        refresh();
    }

    public void SetPartyMax(int partyMax) {
        presence.partyMax = partyMax;
        refresh();
    }

    public void SetMatchSecret(string matchSecret) {
        presence.matchSecret = matchSecret;
        refresh();
    }

    public void SetJoinSecret(string joinSecret) {
        presence.joinSecret = joinSecret;
        refresh();
    }

    public void SetSpectateSecret(string spectateSecret) {
        presence.spectateSecret = spectateSecret;
        refresh();
    }

    public void SetInstance(bool instance) {
        presence.instance = instance;
        refresh();
    }

    public void refresh() {
        if (!running) {
            Startup();
        }
        DiscordRPC.UpdatePresence(presence);
    }
}