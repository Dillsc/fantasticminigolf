﻿using UnityEngine;

public class DiscordPreset : MonoBehaviour {

    [Header("Game State")]
    [Tooltip("The details of what the player is doing")] public string Details = "";
    [Tooltip("The state of the activity the player is doing")] public string State = "";

    [Header("Time Stats")]
    [Tooltip("Duration of the current match/level")] public long StartTime = 0;
    [Tooltip("It will set the timer to zero (Overrides the Start Time)")] public bool SetStartTimeNow = false;
    [Tooltip("The countdown until the end of the match/level in seconds (Overrides the Start Time)")] public int EndTime;

    [Header("Image Settings")]
    [Tooltip("The key of the large image")] public string LargeImageKey = "";
    [Tooltip("The large images tooltip text")] public string LargeImageText = "";
    [Tooltip("The key of the small image")] public string SmallImageKey = "";
    [Tooltip("The small images tooltip text")] public string SmallImageText = "";

    [Header("Party/Multiplayer Settings")]
    public string PartyID = "";
    public int PartySize = 0;
    public int PartyMax = 0;

    public string MatchSecret = "";
    public string JoinSecret = "";
    public string SpectateSecret = "";
    public bool isInstance = false;
}
