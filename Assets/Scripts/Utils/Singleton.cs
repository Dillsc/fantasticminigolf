﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

    private static T instance;

    public static T Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<T>();
                if (instance == null) {
                    GameObject newInstance;
                    if (typeof(T).Name == typeof(PrefabManager).Name) {
                        newInstance = (GameObject) Instantiate(Resources.Load("PrefabManager"));
                        instance = newInstance.GetComponent<T>();
                    } else {
                        Debug.Log(typeof(T).Name.ToLower());
                        newInstance = PrefabManager.Instance.AvailablePrefabs[typeof(T).Name.ToLower()];
                        if (newInstance == null) {
                            newInstance = new GameObject(typeof(T).Name);
                            instance = newInstance.AddComponent<T>();
                        } else {
                            GameObject _newInstance = Instantiate(newInstance);
                            instance = _newInstance.GetComponent<T>();
                        }
                    }
                    Debug.LogWarning("Warning: Singleton of " + newInstance.name + " does not exist.");
                }
            }
            return instance;
        }
    }

    private void Awake() {
        if (Instance != this) {
            Destroy(gameObject);
        }

        OnAwake();
    }

    protected virtual void OnAwake() {
    }
}
