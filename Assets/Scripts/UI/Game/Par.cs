﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using TMPro;

public class Par : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI _strokes;
    [SerializeField] private TextMeshProUGUI _par;

    private void Start() {
        GameManager.Instance._parProperty.Subscribe(val => _par.text = val.ToString()).AddTo(this);
        GameManager.Instance._strokesProperty.Subscribe(val => _strokes.text = val.ToString()).AddTo(this);
    }
}
