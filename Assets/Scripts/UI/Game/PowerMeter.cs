﻿using UniRx;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PowerMeter : MonoBehaviour {

    [SerializeField] private Image _PowerMeter;
    [SerializeField] private Image _PreviousPowerMeter;

    [SerializeField] private RectTransform _PowerMeterPosition;
    [SerializeField] private RectTransform _PrevPowerMeterPosition;

    [SerializeField] private TextMeshProUGUI _PrevPower;
    [SerializeField] private TextMeshProUGUI _Power;

    private Player player;

    private void Start() {
        player = FindObjectOfType<Player>();

        if (player != null) {
            player.PowerReactiveProperty.AsObservable().Subscribe(i => OnPowerChange(i)).AddTo(this);
            player.PreviousPowerReactiveProperty.AsObservable().Subscribe(i => OnPreviousPowerChange(i)).AddTo(this);
        }
    }

    private void OnPowerChange(float power) {
        if (power == 0) {
            _Power.gameObject.SetActive(false);
        } else {
            _Power.gameObject.SetActive(true);
        }

        _PowerMeter.rectTransform.localScale = new Vector3(1, power == 0 ? 0 : power / player.MaxPower, 1);
        _Power.text = Mathf.FloorToInt((power == 0 ? 0 : power / player.MaxPower) * 100) + "%";
        _Power.rectTransform.position = new Vector3(_Power.rectTransform.position.x, _PowerMeterPosition.position.y, _Power.rectTransform.position.z);

        float distance = Mathf.Abs(_PrevPower.rectTransform.position.y - _Power.rectTransform.position.y);

        float changeDistance = 0;

        if (distance < 70) {
            changeDistance = Mathf.Abs(distance - 70);
        }

        _PrevPower.rectTransform.position = new Vector3(_Power.rectTransform.position.x + changeDistance, _PrevPower.rectTransform.position.y, _PrevPower.rectTransform.position.z);
    }

    private void OnPreviousPowerChange(float power) {
        if (power == 0) {
            _PrevPower.gameObject.SetActive(false);
        }
        else {
            _PrevPower.gameObject.SetActive(true);
        }
        _PreviousPowerMeter.rectTransform.localScale = new Vector3(1, power == 0 ? 0 : power / player.MaxPower, 1);
        _PrevPower.text = Mathf.FloorToInt((power == 0 ? 0 : power / player.MaxPower) * 100) + "%";
        _PrevPower.rectTransform.position = new Vector3(_PrevPower.rectTransform.position.x, _PrevPowerMeterPosition.position.y, _PrevPower.rectTransform.position.z);
    }
}
