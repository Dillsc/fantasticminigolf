﻿using UnityEngine;

public class Billboard : MonoBehaviour {

    private Camera _camera;

    private void Start() {
        _camera = FindObjectOfType<Camera>();
    }

    private void Update() {
        transform.LookAt(_camera.transform);
    }
}
