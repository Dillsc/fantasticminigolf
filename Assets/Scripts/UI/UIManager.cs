﻿using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager> {

    [SerializeField] private Canvas canvas = null;
    [SerializeField] private List<RectTransform> Views = null;
    [Space(10)]
    [SerializeField] private RectTransform DefaultView = null;

    private int _currentView;

    protected override void OnAwake() {
        if (DefaultView == null) {
            DefaultView = Views[0];
        }
        if (Views.Contains(DefaultView) == false) {
            Debug.LogWarning("Selected default view is not in the list of views");
            return;
        }

        if (canvas == null) {
            canvas = FindObjectOfType<Canvas>();
        }

        _currentView = Views.IndexOf(DefaultView);

        float width = ((RectTransform) canvas.transform).rect.width;
        float height = ((RectTransform) canvas.transform).rect.height;
        for (int i = 0; i < Views.Count; i++) {
            Views[i].position = new Vector3(width / 2, height / 2);
            if (i == _currentView) {
                Views[i].gameObject.SetActive(true);
            } else {
                Views[i].gameObject.SetActive(false);
            }
        }
    }

    public void OpenView(GameObject newView) {
        RectTransform newViewTransform = newView.GetComponent<RectTransform>();

        if (newViewTransform == null) {
            Debug.LogError("New view \"" + newView.name + "\" does not have a rect transform");
            return;
        }

        if (Views.Contains(newViewTransform) == false) {
            Debug.LogError("New view \"" + newView.name + "\" is not setup to be a view");
            return;
        }

        _currentView = Views.IndexOf(newViewTransform);

        for (int i = 0; i < Views.Count; i++) {
            if (i == _currentView) {
                Views[i].gameObject.SetActive(true);
            } else {
                Views[i].gameObject.SetActive(false);
            }
        }
    }
}