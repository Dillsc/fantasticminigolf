﻿using UnityEngine;
using UnityEngine.UI;

public class QuitButtonYes : InteractiveButton {

    private void Start() {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    private void Click() {
        Application.Quit();
    }
}