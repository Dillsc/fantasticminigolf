﻿using UnityEngine;
using UnityEngine.UI;

public class RGBValueInputs : MonoBehaviour {

    [SerializeField] private float _min = 0;
    [SerializeField] private float _max = 255;

    [SerializeField] private bool _isRed = false;
    [SerializeField] private bool _isGreen = false;
    [SerializeField] private bool _isBlue = false;

    private InputField _field;

    private void Awake() {
        _field = GetComponent<InputField>();
        _field.onValueChanged.AddListener(OnValueChange);
    }

    private void OnValueChange(string value) {
        if (string.IsNullOrEmpty(value)) {
            return;
        }

        float fvalue = float.Parse(value);
        if (fvalue > _max) {
            fvalue = _max;
            _field.text = _max.ToString();
        }

        if (fvalue < _min) {
            fvalue = _min;
            _field.text = _min.ToString();
        }

        if (_isRed == true) {
            GameManager.Instance.PlayerColorRed = fvalue;
        }
        if (_isGreen == true) {
            GameManager.Instance.PlayerColorGreen = fvalue;
        }
        if (_isBlue == true) {
            GameManager.Instance.PlayerColorBlue = fvalue;
        }
    }
}
