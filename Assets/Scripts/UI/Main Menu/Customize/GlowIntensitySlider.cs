﻿using UnityEngine;
using UnityEngine.UI;

public class GlowIntensitySlider : MonoBehaviour {

    private Slider _slider;

    private void Awake() {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(OnValueChange);
    }

    private void OnValueChange(float value) {
        GameManager.Instance.PlayerGlow = value;
    }
}
