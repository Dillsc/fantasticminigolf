﻿using UnityEngine.UI;

public class PlayButton : InteractiveButton {

    private void Start() {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    private void Click() {
        SceneManager.Instance.LoadScene(SceneManager.ForestScene);
    }
}