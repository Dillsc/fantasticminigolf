﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InteractiveButton : MonoBehaviour, ISelectHandler, IPointerEnterHandler {

    [SerializeField] private AudioClip RolloverSound = null;

    private Button _button;

    private void Awake() {
        _button = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (_button.IsInteractable() == true) {
            SoundManager.Instance.PlaySoundAtListener(RolloverSound, new SourceSettings().SetVolume(0.1f));
        }
    }

    public void OnSelect(BaseEventData eventData) {
        if (_button.IsInteractable() == true) {
            SoundManager.Instance.PlaySoundAtListener(RolloverSound, new SourceSettings().SetVolume(0.1f));
        }
    }
}
