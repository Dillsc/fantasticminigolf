﻿using UnityEngine;
using UnityEngine.UI;

public class CustomizeButton : InteractiveButton {

    [SerializeField] private GameObject View = null;

    private void Start() {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    private void Click() {
        UIManager.Instance.OpenView(View);
    }
}