﻿using UnityEngine;

public class MainMenuCameraController : MonoBehaviour {

    private CameraManager _manager;

    private Vector2 _distance = Vector2.one;

    private void Start() {
        _manager = FindObjectOfType<CameraManager>();
        _manager.SetFollowTransform(transform);
        _manager.LockToFirstPersonPerspective = true;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void Update() {
        float x = Input.GetAxis("Mouse X") / Mathf.Max(100, Mathf.Abs(_distance.x * 100));
        float y = Input.GetAxis("Mouse Y") / Mathf.Max(100, Mathf.Abs(_distance.y * 100));

        _distance.x += x * 5;
        _distance.y += y * 5;

        if (_distance.x < -6 || _distance.x > 6) {
            _distance.x = Mathf.Clamp(_distance.x, -6, 6);
            x = 0;
        }

        if (_distance.y < -6 || _distance.y > 6) {
            _distance.y = Mathf.Clamp(_distance.y, -6, 6);
            y = 0;
        }

        _manager.UpdateWithInput(Time.deltaTime, 0f, new Vector3(x, y));
    }
}
