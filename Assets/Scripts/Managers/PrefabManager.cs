﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : Singleton<PrefabManager> {

    public Dictionary<string, GameObject> AvailablePrefabs = new Dictionary<string, GameObject>();

    [SerializeField] private GameObject _gameManager;
    [SerializeField] private GameObject _soundManager;
    [SerializeField] private GameObject _sceneManager;

    protected override void OnAwake() {
        DontDestroyOnLoad(gameObject);
        AvailablePrefabs.Add("GameManager".ToLower(), _gameManager);
        AvailablePrefabs.Add("SoundManager".ToLower(), _soundManager);
        AvailablePrefabs.Add("SceneManager".ToLower(), _sceneManager);
    }
}
