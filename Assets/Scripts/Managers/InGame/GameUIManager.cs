﻿using UnityEngine;
using TMPro;

[System.Obsolete("Re-write")]
public class GameUIManager : Singleton<GameUIManager> {

    [Header("Components")]
    public RectTransform PowerScale;
    public RectTransform PreviousPowerScale;
    public TextMeshProUGUI PowerScaleOutput;

    public void SetPreviousPower(float power, float maxPower) {
        PreviousPowerScale.localScale = new Vector3(1, power / maxPower, 1);
    }

    public void SetPower(float power, float maxPower) {
        PowerScale.localScale = new Vector3(1, power / maxPower, 1);
        PowerScaleOutput.color = new Color(255, 255, 255, power / 10);
        PowerScaleOutput.transform.position = new Vector3(PowerScaleOutput.transform.position.x, Mathf.Max(5.6f, PowerScale.rect.size.y * PowerScale.transform.localScale.y), 0);
        PowerScaleOutput.text = Mathf.CeilToInt((power / maxPower) * 100) + "%";
    }
}
