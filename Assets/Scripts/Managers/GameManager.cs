﻿using System.Collections;
using UnityEngine;
using UniRx;

public enum GameManagerState {
    Splash,
    MainMenu,
    Loading,
    InGame
}

public class GameManager : Singleton<GameManager> {

    [Header("Music")]
    [SerializeField] private AudioClip MainMenuMusic = null;
    [SerializeField] private AudioClip ForestMusic = null;

    [Header("Other")]
    [SerializeField] private Material PlayerBallMaterial = null;

    private GameManagerState _currentState;
    public ReactiveProperty<int> _parProperty { get; } = new ReactiveProperty<int>(0);
    public ReactiveProperty<int> _strokesProperty { get; } = new ReactiveProperty<int>(0);
    public ReactiveProperty<int> _maxStrokesProperty { get; } = new ReactiveProperty<int>(0);

    public int Par { get { return _parProperty.Value; } set { _parProperty.Value = value; } }
    public int Strokes { get { return _strokesProperty.Value; } set { _strokesProperty.Value = value; } }
    public int MaxStrokes { get { return _maxStrokesProperty.Value; } set { _maxStrokesProperty.Value = value; } }

    public float PlayerColorRed {
        get {
            return PlayerBallMaterial.color.r;
        }
        set {
            PlayerBallMaterial.color = new Color(value, PlayerColorGreen, PlayerColorBlue);
        }
    }
    public float PlayerColorGreen {
        get {
            return PlayerBallMaterial.color.g;
        }
        set {
            PlayerBallMaterial.color = new Color(PlayerColorRed, value, PlayerColorBlue);
        }
    }
    public float PlayerColorBlue {
        get {
            return PlayerBallMaterial.color.b;
        }
        set {
            PlayerBallMaterial.color = new Color(PlayerColorRed, PlayerColorGreen, value);
        }
    }

    public float PlayerGlow {
        get {
            return PlayerBallMaterial.GetFloat("_EmissionScaleUI");
        }
        set {
            return;
            if (value == 0) {
                PlayerBallMaterial.SetFloat("_EmissionScaleUI", 0);
            } else {
                PlayerBallMaterial.SetFloat("_EmissionScaleUI", value / 100);
            }
        }
    }

    private void OnDrawGizmos() {
        Gizmos.DrawIcon(transform.position, "GameManager");
    }

    protected override void OnAwake() {
        SaveManager.LoadConfig();
        DontDestroyOnLoad(gameObject);
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += Init;
    }

    private void Init(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode mode) {
        if (scene.name == SceneManager.LoadingScene.SceneName) {
            _currentState = GameManagerState.Loading;
        } else if (scene.name == SceneManager.SplashScene.SceneName) {
            _currentState = GameManagerState.Splash;
        } else if (scene.name == SceneManager.MainMenuScene.SceneName) {
            _currentState = GameManagerState.MainMenu;
        } else {
            _currentState = GameManagerState.InGame;
        }

        StartCoroutine(SetupState());
    }

    private IEnumerator SetupState() {
        yield return new WaitForEndOfFrame();
        EnterState();
    }

    private void EnterState() {
        switch (_currentState) {
            case GameManagerState.Splash: {
                StartCoroutine(ExitFromSplashState(SceneManager.MainMenuScene, 8f));
                break;
            }
            case GameManagerState.Loading: {
                break;
            }
            case GameManagerState.MainMenu: {
                SoundManager.Instance.PlaySoundAtListener(MainMenuMusic, new SourceSettings().SetVolume(0.5f));
                break;
            }
            case GameManagerState.InGame: {
                SoundManager.Instance.PlaySoundAtListener(ForestMusic, new SourceSettings().SetVolume(0.5f));

                GameObject start = GameObject.FindGameObjectWithTag("PlayerStart");
                HoleDetails details = start.transform.parent.GetComponentInParent<HoleDetails>();
                Player player = FindObjectOfType<Player>();

                player.transform.position = start.transform.position;
                player.transform.rotation = start.transform.rotation;
                Par = details.Par;
                MaxStrokes = details.MaxStrokes;

                Strokes = 0;

                break;
            }
        }
    }

    private IEnumerator ExitFromSplashState(Scene nextScene, float time) {
        yield return new WaitForSeconds(time);

        SceneManager.Instance.LoadScene(nextScene);
    }

    public void SetHoleDetails(HoleDetails details) {
        Par = details.Par;
        MaxStrokes = details.MaxStrokes;
    }

    public static void Quit() {
        SaveManager.SaveConfig();
        Application.Quit();
    }
}