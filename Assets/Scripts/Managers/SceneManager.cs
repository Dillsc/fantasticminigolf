﻿using System;
using System.Collections;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneManager : Singleton<SceneManager> {

    public static Scene LoadingScene {
        get {
            return Instance._loadingScene;
        }
    }
    public static Scene MainMenuScene {
        get {
            return Instance._mainMenuScene;
        }
    }
    public static Scene SplashScene {
        get {
            return Instance._splashScene;
        }
    }
    public static Scene TestEnvironmentScene {
        get {
            return Instance._testEnvironmentScene;
        }
    }
    public static Scene ForestScene {
        get { 
            return Instance._forestScene;
        }
    }

    [Header("Loading Scene")]
    [SerializeField] private Scene _loadingScene = null;

    [Header("Non-Level Scenes")]
    [SerializeField] private Scene _mainMenuScene = null;
    [SerializeField] private Scene _splashScene = null;

    [Header("Level Scenes")]
    [SerializeField] private Scene _testEnvironmentScene = null;
    [SerializeField] private Scene _forestScene = null;

    protected override void OnAwake() {
        DontDestroyOnLoad(gameObject);
    }

    public void LoadSceneAsync(Scene scene) {
        SoundManager.Instance.StopAllInUseSources();
        UnityEngine.SceneManagement.SceneManager.LoadScene(_loadingScene);
        StartCoroutine(LoadSceneInternal(scene));
    }

    public void LoadSceneDirectAsync(Scene scene) {
        SoundManager.Instance.StopAllInUseSources();
        StartCoroutine(LoadSceneInternal(scene));
    }

    public void LoadScene(Scene scene) {
        SoundManager.Instance.StopAllInUseSources();
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }

    private IEnumerator LoadSceneInternal(Scene scene, float delay = 0.1f) {
        if (delay > 0) {
            yield return new WaitForSeconds(delay);
        }

        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene);

        yield return new WaitUntil(() => async.isDone);
    }
}

#region Scene Field
[Serializable]
public class Scene {
    [SerializeField] private UnityEngine.Object m_SceneAsset;
    [SerializeField] private string m_SceneName = "";

    public string SceneName => m_SceneName;

    public static implicit operator string(Scene sceneField) {
        return sceneField.SceneName;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(Scene))]
public class SceneFieldPropertyDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, GUIContent.none, property);
        SerializedProperty sceneAsset = property.FindPropertyRelative("m_SceneAsset");
        SerializedProperty sceneName = property.FindPropertyRelative("m_SceneName");
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        if (sceneAsset != null) {
            sceneAsset.objectReferenceValue = EditorGUI.ObjectField(position, sceneAsset.objectReferenceValue, typeof(SceneAsset), false);
            if (sceneAsset.objectReferenceValue != null) {
                sceneName.stringValue = (sceneAsset.objectReferenceValue as SceneAsset).name;
            }
        }

        EditorGUI.EndProperty();
    }
}
#endif
#endregion