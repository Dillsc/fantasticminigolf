﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraManager : Singleton<CameraManager> {

    [Header("Framing")]
    public Vector2 FollowTransformFraming = new Vector2(0f, 0f);
    public float FollowingSharpness = 30f;

    [Header("Distance")]
    public bool LockToFirstPersonPerspective = false;
    public float DefaultDistance = 6f;
    public float MinDistance = 0f;
    public float MaxDistance = 10f;
    public float MovementSpeed = 10f;
    public float MovementSharpness = 10f;

    [Header("Rotation")]
    public bool InvertX = false;
    public bool InvertY = false;

    [Range(-90f, 90f)] public float DefaultVerticalAngle = 20f;
    [Range(-90f, 90f)] public float MinVerticalAngle = -80f;
    [Range(-90f, 90f)] public float MaxVerticalAngle = 80f;

    public float RotationSpeed = 10f;
    public float RotationSharpness = 30f;

    [Header("Obstruction Feature")]
    public float ObstructionCheckRadius = 0.5f;
    public LayerMask ObstructionLayers = -1;
    public float ObstructionSharpness = 10000f;

    public Transform Transform { get; private set; }
    public Vector3 PlanarDirection { get; private set; }
    public Transform FollowTransform { get; set; }
    public Collider[] IgnoredColliders { get; set; }
    public float TargetDistance { get; set; }

    private bool distanceIsObstructed;
    private float currentDistance;
    private float targetVerticalAngle;
    private RaycastHit obstructionHit;
    private int obstructionCount;
    private RaycastHit[] obstructions = new RaycastHit[MaxObstructions];
    private float obstructionTime;
    private Vector3 currentFollowPosition;

    private const int MaxObstructions = 32;

    void OnValidate() {
        DefaultDistance = Mathf.Clamp(DefaultDistance, MinDistance, MaxDistance);
        DefaultVerticalAngle = Mathf.Clamp(DefaultVerticalAngle, MinVerticalAngle, MaxVerticalAngle);
    }

    void Awake() {
        Transform = this.transform;

        if (LockToFirstPersonPerspective) {
            currentDistance = 0;
        } else {
            currentDistance = DefaultDistance;
        }
        TargetDistance = currentDistance;

        targetVerticalAngle = 0f;

        PlanarDirection = Vector3.forward;
    }

    public void SetFollowTransform(Transform followTransform) {
        FollowTransform = followTransform;
        PlanarDirection = followTransform.forward;
        currentFollowPosition = FollowTransform.position;
    }

    public void UpdateWithInput(float deltaTime, float zoomInput, Vector3 rotationInput) {
        if (InvertX) {
            rotationInput.x *= -1f;
        }
        if (InvertY) {
            rotationInput.y *= -1f;
        }

        Quaternion rotationFromInput = Quaternion.Euler(FollowTransform.up * (rotationInput.x * RotationSpeed));
        PlanarDirection = rotationFromInput * PlanarDirection;
        PlanarDirection = Vector3.Cross(FollowTransform.up, Vector3.Cross(PlanarDirection, FollowTransform.up));
        targetVerticalAngle -= (rotationInput.y * RotationSpeed);
        targetVerticalAngle = Mathf.Clamp(targetVerticalAngle, MinVerticalAngle, MaxVerticalAngle);

        if (distanceIsObstructed && Mathf.Abs(zoomInput) > 0f) {
            TargetDistance = currentDistance;
        }
        TargetDistance += zoomInput * MovementSpeed;
        TargetDistance = Mathf.Clamp(TargetDistance, MinDistance, MaxDistance);

        currentFollowPosition = Vector3.Lerp(currentFollowPosition, FollowTransform.position, 1f - Mathf.Exp(-FollowingSharpness * deltaTime));

        Quaternion planarRot = Quaternion.LookRotation(PlanarDirection, FollowTransform.up);
        Quaternion verticalRot = Quaternion.Euler(targetVerticalAngle, 0, 0);
        Quaternion targetRotation = Quaternion.Slerp(Transform.rotation, planarRot * verticalRot, 1f - Mathf.Exp(-RotationSharpness * deltaTime));

        Transform.rotation = targetRotation;

        {
            RaycastHit closestHit = new RaycastHit();
            closestHit.distance = Mathf.Infinity;
            obstructionCount = Physics.SphereCastNonAlloc(currentFollowPosition, ObstructionCheckRadius, -Transform.forward, obstructions, TargetDistance, ObstructionLayers, QueryTriggerInteraction.Ignore);
            for (int i = 0; i < obstructionCount; i++) {
                bool isIgnored = false;
                if (IgnoredColliders != null) {
                    for (int j = 0; j < IgnoredColliders.Length; j++) {
                        if (IgnoredColliders[j] == obstructions[i].collider) {
                            isIgnored = true;
                            break;
                        }
                    }
                }

                if (!isIgnored && obstructions[i].distance < closestHit.distance && obstructions[i].distance > 0) {
                    closestHit = obstructions[i];
                }
            }

            if (closestHit.distance < Mathf.Infinity) {
                distanceIsObstructed = true;
                currentDistance = Mathf.Lerp(currentDistance, closestHit.distance, 1 - Mathf.Exp(-ObstructionSharpness * deltaTime));
            } else {
                distanceIsObstructed = false;
                currentDistance = Mathf.Lerp(currentDistance, TargetDistance, 1 - Mathf.Exp(-MovementSharpness * deltaTime));
            }
        }

        Vector3 targetPosition = currentFollowPosition - ((targetRotation * Vector3.forward) * currentDistance);

        targetPosition += Transform.right * FollowTransformFraming.x;
        targetPosition += Transform.up * FollowTransformFraming.y;

        Transform.position = targetPosition;
    }
}