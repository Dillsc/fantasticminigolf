﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

public enum PreferenceType {
    General,
    Preference
}

public struct SaveManager {

    private const string GENERAL_SLUG = "[General]";
    private const string PREFERENCES_SLUG = "[Preferences]";

    private static string _Documents_Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\My Games\\" + Application.productName + "\\";
    private static string _Preferences_File = _Documents_Path + "Preferences.ini";

    private static Dictionary<string, object> _GeneralFlags = new Dictionary<string, object>();
    private static Dictionary<string, object> _PreferencesFlags = new Dictionary<string, object>();

    public static void SetPref(string key, object value, PreferenceType type) {
        switch (type) {
            case PreferenceType.General:
                _GeneralFlags.Add(key, value);
                break;
            case PreferenceType.Preference:
                _PreferencesFlags.Add(key, value);
                break;
        }
    }

    public static T GetPref<T>(string key) {
        if (_GeneralFlags.ContainsKey(key)) {
            return (T) _GeneralFlags[key];
        }
        if (_PreferencesFlags.ContainsKey(key)) {
            return (T) _PreferencesFlags[key];
        }
        return default;
    }

    public static T GetPref<T>(string key, PreferenceType type) {
        switch (type) {
            case PreferenceType.General:
                return (T) _GeneralFlags[key];
            case PreferenceType.Preference:
                return (T) _PreferencesFlags[key];
            default:
                return default;
        }
    }

    public static void LoadConfig() {
        if (File.Exists(_Preferences_File) == false) {
            return;
        }
    }

    public static void SaveConfig() {
        if (File.Exists(_Preferences_File) == false) {
            Directory.CreateDirectory(_Documents_Path);
            File.Create(_Preferences_File);
            File.SetAttributes(_Preferences_File, FileAttributes.Normal);
        }
        
        StreamWriter writer = new StreamWriter(_Preferences_File, false, Encoding.ASCII);

        writer.WriteLine(GENERAL_SLUG);

        foreach (string key in _GeneralFlags.Keys) {
            writer.WriteLine(key + " = " + _GeneralFlags[key]);
        }

        writer.WriteLine(PREFERENCES_SLUG);

        foreach (string key in _PreferencesFlags.Keys) {
            writer.WriteLine(key + " = " + _PreferencesFlags[key]);
        }

        writer.Close();
    }

}
