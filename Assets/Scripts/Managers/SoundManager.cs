﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class SoundManager : Singleton<SoundManager> {

    private const int MAX_SOURCES = 32;

    [SerializeField] private AudioMixerGroup Mixer = null;

    private Queue<AudioSource> _availableSources = new Queue<AudioSource>();
    private List<AudioSource> _sourcesInUse = new List<AudioSource>();
    private AudioListener _audioListener;

    protected override void OnAwake() {
        DontDestroyOnLoad(gameObject);

        Init();

        UnityEngine.SceneManagement.SceneManager.sceneLoaded += FindNewAudioListener;
    }

    private void FindNewAudioListener(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode) {
        _audioListener = FindObjectOfType<AudioListener>();
    }

    public void PlaySoundAtListener(AudioClip clip, SourceSettings settings = null) {
        AudioSource source = GetAvailableSource();
        source.transform.parent = _audioListener.transform;
        source.clip = clip;
        Play(source, settings);
    }

    public void PlaySoundAtObject(GameObject obj, AudioClip clip, SourceSettings settings = null) {
        AudioSource source = GetAvailableSource();
        source.transform.parent = obj.transform;
        source.transform.position = Vector3.zero;
        source.clip = clip;
        Play(source, settings);
    }

    public void PlaySoundAtLocation(Vector3 location, AudioClip clip, SourceSettings settings = null) {
        AudioSource source = GetAvailableSource();
        source.transform.parent = null;
        source.transform.position = location;
        source.clip = clip;
        Play(source, settings);
    }

    public void PlaySoundAtSource(AudioSource source, AudioClip clip) {
        source.clip = clip;
        source.Play();
        StartCoroutine(UnloadSource(source, true));
    }

    private void Play(AudioSource source, SourceSettings settings) {
        source.outputAudioMixerGroup = Mixer;
        settings?.ApplySettings(ref source);

        source.Play();

        StartCoroutine(UnloadSource(source));
    }

    public void StopAllInUseSources() {
        List<AudioSource> copy = new List<AudioSource>(_sourcesInUse);
        foreach (AudioSource source in copy) {
            RenewSource(source);
        }
    }

    private IEnumerator UnloadSource(AudioSource source, bool external = false) {
        yield return new WaitWhile(() => source.isPlaying);

        source.Stop();
        if (external == false) {
            RenewSource(source);
        }
    }

    private void RenewSource(AudioSource source) {
        source.clip = null;
        source.transform.parent = gameObject.transform;
        source.transform.position = Vector3.zero;
        _sourcesInUse.Remove(source);
        _availableSources.Enqueue(source);
    }

    private AudioSource GetAvailableSource() {
        if (_availableSources.Count == 0) {
            Debug.LogWarning("Audio source pool empty!");
            Init();
        }
        AudioSource source = _availableSources.Dequeue();
        _sourcesInUse.Add(source);
        return source;
    }

    private void Init() {
        _audioListener = FindObjectOfType<AudioListener>();

        for (int i = 0; i < MAX_SOURCES; i++) {
            GameObject sourceObject = new GameObject("Audio Source " + i);
            sourceObject.transform.parent = gameObject.transform;
            AudioSource source = sourceObject.AddComponent<AudioSource>();
            _availableSources.Enqueue(source);
        }
    }
}

public class SourceSettings {

    public float Priority { get; private set; }
    public float Volume { get; private set; }
    public float Pitch { get; private set; }
    public float MaxPitch { get; private set; }
    public float MinPitch { get; private set; }
    public float PanStereo { get; private set; }
    public float SpartialBlend { get; private set; }
    public float ReverbZoneMix { get; private set; }
    public float DopplerLevel { get; private set; }
    public float Spread { get; private set; }
    public AudioRolloffMode RolloffMode { get; private set; }
    public float MinDistance { get; private set; }
    public float MaxDistance { get; private set; }

    public SourceSettings() {
        Volume = 1.0f;
        Pitch = 1.0f;
        MaxPitch = 0.0f;
        MinPitch = 0.0f;
        PanStereo = 0.0f;
        SpartialBlend = 0.0f;
        ReverbZoneMix = 1.0f;
        DopplerLevel = 1.0f;
        Spread = 0.0f;
        RolloffMode = AudioRolloffMode.Logarithmic;
        MinDistance = 1.0f;
        MaxDistance = 500.0f;
    }

    public void ApplySettings(ref AudioSource source) {
        source.volume = Volume;
        source.pitch = (MaxPitch == MinPitch) ? Pitch : Random.Range(MinPitch, MaxPitch);
        source.panStereo = PanStereo;
        source.spatialBlend = SpartialBlend;
        source.reverbZoneMix = ReverbZoneMix;
        source.dopplerLevel = DopplerLevel;
        source.spread = Spread;
        source.rolloffMode = RolloffMode;
        source.minDistance = MinDistance;
        source.maxDistance = MaxDistance;
    }

    public SourceSettings SetVolume(float volume) {
        Volume = volume;
        return this;
    }

    public SourceSettings SetPitch(float pitch) {
        Pitch = pitch;
        return this;
    }

    public SourceSettings SetMaxPitch(float maxPitch) {
        MaxPitch = maxPitch;
        return this;
    }

    public SourceSettings SetMinPitch(float minPitch) {
        MinPitch = minPitch;
        return this;
    }

    public SourceSettings SetPanStereo(float panStereo) {
        PanStereo = panStereo;
        return this;
    }

    public SourceSettings SetSpartialBlend(float spartialBlend) {
        SpartialBlend = spartialBlend;
        return this;
    }

    public SourceSettings SetReverbZoneMix(float reverbZoneMix) {
        ReverbZoneMix = reverbZoneMix;
        return this;
    }

    public SourceSettings SetDopplerLevel(float dopplerLevel) {
        DopplerLevel = dopplerLevel;
        return this;
    }

    public SourceSettings SetSpread(float spread) {
        Spread = spread;
        return this;
    }

    public SourceSettings SetRolloffMode(AudioRolloffMode rolloffMode) {
        RolloffMode = rolloffMode;
        return this;
    }

    public SourceSettings SetMinDistance(float minDistance) {
        MinDistance = minDistance;
        return this;
    }

    public SourceSettings SetMaxDistance(float maxDistance) {
        MaxDistance = maxDistance;
        return this;
    }
}